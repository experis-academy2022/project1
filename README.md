# Computer Shop


Computer Shop is an app in which user can buy imaginary laptops. The app also includes very primitive online bank. This is the first assignment in Experis Academy and was made with vanilla JavaScript.

<br>



## Features

The user is able to:

* Buy laptops from a menu
* Use online bank service

<br>


## Contributor
- Marie Puhakka     https://gitlab.com/mariesusan


<br>

## License
[MIT](https://choosealicense.com/licenses/mit/)
