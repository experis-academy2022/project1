
const bankBalanceElement = document.getElementById("bankBalance")
const loanElement = document.getElementById("loan")
const getALoanButtonElement = document.getElementById("getALoanButton")
const currentSalaryElement = document.getElementById("currentSalary")
// trasfers all the money from salary to bank, if loan, 10% of salary first to pay the loan, 90% to bank
// remember to reset salary to 0
const bankButtonElement = document.getElementById("bankButton")
// increases salary 100/click
const workButtonElement = document.getElementById("workButton")
// transfers all the money from salary to loan, if money left transfers to bank
const repayButtonElement = document.getElementById("repayButton")
// API-> computers, for pics own path
const computersElement = document.getElementById("computers")
const featuresElement = document.getElementById("featuresLaptop")
// const infoElement = document.getElementById("infoLaptop")
// money from the bank, message you are the owner of the new laptop
// if not enough money, message can't afford the laptop
const buyNowButtonElement = document.getElementById("buyNowButton")

const computerPicElement = document.getElementById("computerPic")
const computerNameElement = document.getElementById("computerName")
const computerDescriptionElement = document.getElementById("computerDescription")
const computerPriceElement = document.getElementById("computerPrice")


let computers = []

//computers from API
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => computers = data)
    .then(computers => addComputersToMenu(computers))


const addComputersToMenu = (computers) => {
    computers.forEach(x => addComputerToMenu(x))
}

const addComputerToMenu = (computer) => {
    const computerElement = document.createElement("option")
    computerElement.value = computer.id
    computerElement.appendChild(document.createTextNode(computer.title))
    computersElement.appendChild(computerElement)
}

const handleComputerMenuChange = e => {
    const selectedComputer = computers[e.target.selectedIndex]
    // featuresElement.innerText = selectedComputer.specs
    featuresElement.innerText = computerSpecs(selectedComputer)
    // name
    computerNameElement.innerText = selectedComputer.title
    // description
    computerDescriptionElement.innerText = selectedComputer.description
    // price
    computerPriceElement.innerText = selectedComputer.price + " NOK"
    // buy now -button
    buyNowButtonElement


    //computer image from API: for some reason doesn't work 
    const backup_pic = "https://noroff-komputer-store-api.herokuapp.com/assets/images/1.png"
    fetch("https://noroff-komputer-store-api.herokuapp.com/" + selectedComputer.image)
        .then(response => response.json())
        .then( j => { 
            if (j.url.includes(".mp4")) {
                console.error("Image contains mp4, which sucks!")
            } else {
                return j;
            }
        })
        .then( j => computerPicElement.src = j.url)
        .catch( err => {
            console.error("Failed to fetch a computer pic!");
            computerPicElement.src = backup_pic
        })
        
}

computersElement.addEventListener("change", handleComputerMenuChange)


const computerSpecs = (selectedComputer) => {
    let specsDataRaw = selectedComputer.specs
    let specsDataNew = ""
    for (let i = 0; i < specsDataRaw.length; i++) {
        specsDataNew += specsDataRaw[i] + "\n"
    }
    return specsDataNew
}


// event listener to buttons
getALoanButtonElement.addEventListener("click", handleGetALoanButton)
bankButtonElement.addEventListener("click", handleBankButton)
workButtonElement.addEventListener("click", handleWorkButton)
repayButtonElement.addEventListener("click", handleRepayButton)
buyNowButtonElement.addEventListener("click", handleBuyNowButton)

const handleGetALoanButton = (e) => {
    
}

const handleBankButton = (e) => {

}   

const handleWorkButton = (e) => {

}

const handleRepayButton = (e) => {

}

const handleBuyNowButton = (e) => {

}



